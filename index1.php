<html>
<head>
	<title>Huawei Solutions</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="style.css" />

	<link rel="icon" type="image/png" href="favicon.png">
	<link rel="shortcut icon" href="favicon.ico">

	<script type="text/javascript">
		document.createElement('header');
		document.createElement('article');
		document.createElement('footer');
		document.createElement('section');
		document.createElement('aside');
		document.createElement('nav');
	</script>
	
	<script type="text/javascript" src="scripts.min.js"></script>

	<!-- Meta Tags and Open Graph -->
	<meta property="og:title" content="Huawei Solutions" />

	<!-- Additional Tags -->
</head>
<body>
	<header>

		<div id="cnn-next-header" class="cnn-next-element">
			<div>
				<a href="http://www.cnn.com">
					<img src="http://i.cdn.turner.com/cnn/tcp/adfeatures/cnn-next-header-logo.gif" alt="CNN" />
					<span><img src="http://i.cdn.turner.com/cnn/tcp/adfeatures/cnn-next-backwards-arrow.gif" alt="CNN" />Back to CNN content</span>
				</a>
			</div>
		</div>
		<div class="cnn-next-element cnn-next-disclaimer">
			<div>
				Sponsor Content from <img src="logo.png?color=255,255,255" alt="" />
			</div>
		</div>
	</header>
	<article id="overview-zone">
		<header>
			<h1>Huawei Solutions</h1>
		</header>
		<div class="background empty"></div>
		<ul class="featured">
			<li style="background-image: url(header.jpg);">
				<a href="./AI-in-manufacturing/">
					<h3>AI in manufacturing: Ready for Impact</h3>
				</a>
			</li>
			<li style="background-image: url(header1.jpg);">
				<a href="./Driving-AI/">
					<h3>Driving AI: Connected Cars' Highway to the Future</h3>
				</a>
			</li>
		</ul>
		<section>
			<p class="socials">
				<a href="#" data-share="facebook">Facebook</a>
					<a href="#" data-share="twitter">Twitter</a>
					<a href="#" data-share="email">Email</a>
				</p>
			<p class="byline">
				<img src="logo.png" /> for CNN
			</p>
			<p class="date">
				Updated 04:05 CET October 10, 2018
			</p>
		</section>
		<ul>
			<li style="background-image: url(thumbnail.jpg);">
				<a href="./Smart-cities/">
					<h3>Smart cities: Applying intelligence to urban growth</h3>
				</a>
			</li>
			<li style="background-image: url(thumbnail1.jpg);">
				<a href="./Dawning-of-the-age-of-AI/">
					<h3>Dawning of the age of AI</h3>
				</a>
			</li>
		</ul>
	</article>
	<footer>
		<div class="cnn-next-element cnn-next-disclaimer">
			<div>
				Sponsor Content from <img src="logo.png?color=255,255,255" alt="Huawei" />
			</div>
		</div>
		<div id="cnn-next-footer" class="cnn-next-element">
			<div>
				<img src="http://i.cdn.turner.com/cnn/tcp/adfeatures/cnn-next-footer-logo.png" alt="CNN" />
				<p>
					&copy; 2017 Cable News Network. Turner Broadcasting System Europe, limited. All Rights Reserved.
				</p>
				<ul>
					<li><a href="http://edition.cnn.com/terms">Terms of service</a></li>
					<li><a href="http://edition.cnn.com/privacy">Privacy guidelines</a></li>
					<li><a href="http://edition.cnn.com/#">Ad choices</a></li>
					<li><a href="http://edition.cnn.com/about">About us</a></li>
					<li><a href="http://edition.cnn.com/feedback">Contact us</a></li>
					<li><a href="http://www.turner.com/careers">Work for us</a></li>
					<li><a href="http://edition.cnn.com/help">Help</a></li>
				</ul>
			</div>
		</div>
	</footer>

	<script src="http://assets.adobedtm.com/163d1096ee58e69f3c2853388c3bd41996e5fc4f/satelliteLib-d76d1730abb96687bfefdfdb53da02b40150601c.js"></script>	<script type="text/javascript">_satellite.pageBottom();</script>	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69834693-2', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</htmL><!-- Empty Template -->
